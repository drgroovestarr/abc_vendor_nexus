#
# This policy configuration will be used by all products that
# inherit from pure
#

BOARD_PLAT_PUBLIC_SEPOLICY_DIR += \
    vendor/nexus/sepolicy/public

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    vendor/nexus/sepolicy/private
